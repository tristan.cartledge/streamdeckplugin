package logging

import (
	"fmt"

	"meow.tf/streamdeck/sdk"
)

func Info(message string, args ...interface{}) {
	sdk.Log(fmt.Sprintf("INFO: %s", fmt.Sprintf(message, args...)))
}

func Error(message string, args ...interface{}) {
	sdk.Log(fmt.Sprintf("ERROR: %s", fmt.Sprintf(message, args...)))
}
