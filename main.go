package main

import (
	"fmt"
	"log"

	"gitlab.com/tristan.cartledge/streamdeckplugin/actions"
	"gitlab.com/tristan.cartledge/streamdeckplugin/logging"

	"github.com/valyala/fastjson"
	"meow.tf/streamdeck/sdk"
)

const reverseDNS = "com.tristancartledge.plugin"

type actionID string

func (a actionID) String() string {
	return fmt.Sprintf("%s.%s", reverseDNS, string(a))
}

const (
	actionExec = actionID("execaction")
)

type action interface {
	Handle(payload string)
	ReceiveData(data string)
	ReceiveSettings(settings string)
}

var actionFactory = map[string]func(string, string) action{
	actionExec.String(): func(context, settings string) action {
		return actions.NewExec(context, settings)
	},
}

var actionInstances = map[string]action{}

func main() {
	// Initialize handlers for events
	sdk.AddHandler(onGlobalSettings)
	sdk.AddHandler(onWillAppear)
	sdk.AddHandler(onWillDisappear)
	sdk.AddHandler(onReceiveSettings)
	sdk.AddHandler(onSendToPlugin)

	// Register Plugin's actions
	sdk.RegisterAction(actionExec.String(), actionHandler)

	// Open and connect the SDK
	if err := sdk.Open(); err != nil {
		log.Fatalln(err)
	}

	// Wait until the socket is closed, or SIGTERM/SIGINT is received
	sdk.Wait()
}

func onGlobalSettings(e *sdk.GlobalSettingsEvent) {
	sdk.Log("GlobalSettingsEvent")
	sdk.Log(e.Settings.String())
}

func onWillAppear(e *sdk.WillAppearEvent) {
	// TODO logging library
	logging.Info("WillAppearEvent - Action: %s, Context: %s", e.Action, e.Context)
	logging.Info(e.Payload.String())

	newfunc, ok := actionFactory[e.Action]
	if !ok {
		logging.Error("WillAppearEvent - Unknown Action: %s", e.Action)
		return
	}

	actionInstances[e.Context] = newfunc(e.Context, e.Payload.Get("settings").String())
}

func onWillDisappear(e *sdk.WillDisappearEvent) {
	logging.Info("WillDisappearEvent - Action: %s, Context: %s", e.Action, e.Context)
	logging.Info(e.Payload.String())

	delete(actionInstances, e.Context)
}

func onReceiveSettings(e *sdk.ReceiveSettingsEvent) {
	logging.Info("ReceiveSettingsEvent - Action: %s, Context: %s", e.Action, e.Context)

	actionInst := findAction(e.Context)
	if actionInst == nil {
		logging.Error("ReceiveSettingsEvent - Unknown Context: %s, Action: %s", e.Context, e.Action)
		return
	}

	actionInst.ReceiveSettings(e.Settings.String())
}

func onSendToPlugin(e *sdk.SendToPluginEvent) {
	logging.Info("SendToPluginEvent - Action: %s, Context: %s", e.Action, e.Context)
	logging.Info(e.Payload.String())

	actionInst := findAction(e.Context)
	if actionInst == nil {
		logging.Error("SendToPluginEvent - Unknown Context: %s, Action: %s", e.Context, e.Action)
		return
	}

	actionInst.ReceiveData(e.Payload.String())
}

func actionHandler(action, context string, payload *fastjson.Value, deviceID string) {
	logging.Info("ActionHadler - Action: %s, Context: %s", action, context)
	logging.Info(payload.String())

	actionInst := findAction(context)
	if actionInst == nil {
		logging.Error("ActionHadler - Unknown Context: %s, Action: %s", context, action)
		return
	}

	actionInst.Handle(payload.String())
}

func findAction(context string) action {
	actionInst, ok := actionInstances[context]
	if !ok {
		return nil
	}

	return actionInst
}
