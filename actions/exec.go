package actions

import (
	"encoding/json"

	"gitlab.com/tristan.cartledge/streamdeckplugin/logging"

	"meow.tf/streamdeck/sdk"
)

type settings struct {
	Executable *string `json:"executable,omitempty"`
	Arguments  *string `json:"arguments,omitempty"`
}

type ExecAction struct {
	context    string
	executable string
	arguments  string
}

func NewExec(context string, settings string) *ExecAction {
	action := &ExecAction{
		context: context,
	}
	action.ReceiveSettings(settings)

	return action
}

func (e *ExecAction) Handle(payload string) {
	/*data, err := ioutil.ReadFile("icon.png")
	if err != nil {
		logging.Error("failed reading icon: %v", err)
		return
	}

	logging.Info("setting image")
	sdk.SetImage(e.context, fmt.Sprintf("data:image/png;base64,%s", base64.StdEncoding.EncodeToString(data)), 0)*/

	logging.Info(payload)

	// TODO may need to split arguments on space
	cmd := GetCommand(e.executable, e.arguments)
	err := cmd.Start()
	if err != nil {
		logging.Error("failed to start executable: %v", err)
		return
	}
	logging.Info("successfully started executable: %s", cmd.String())
}

func (e *ExecAction) ReceiveData(eventRaw string) {
	event := struct {
		EventType string          `json:"eventType"`
		Data      json.RawMessage `json:"data"`
	}{}

	err := json.Unmarshal([]byte(eventRaw), &event)
	if err != nil {
		logging.Error("failed to unmarshal event: %v", err)
	}

	switch event.EventType {
	case "fieldChange":
		err := e.UnmarshalData(event.Data)
		if err != nil {
			logging.Error("failed to unmarshal data: %v", err)
		}

		sdk.SetSettings(e.context, settings{
			Executable: &e.executable,
			Arguments:  &e.arguments,
		})

		break
	default:
		logging.Error("unknown eventType: %s", event.EventType)
	}
}

func (e *ExecAction) ReceiveSettings(settingsRaw string) {
	err := e.UnmarshalData([]byte(settingsRaw))
	if err != nil {
		logging.Error("failed to unmarshal settings: %v", err)
	}
}

func (e *ExecAction) UnmarshalData(dataRaw []byte) error {
	data := settings{}

	err := json.Unmarshal(dataRaw, &data)
	if err != nil {
		return err
	}

	if data.Executable != nil {
		e.executable = *data.Executable
	}

	if data.Arguments != nil {
		e.arguments = *data.Arguments
	}

	return err
}
