// +build windows

package actions

import (
	"os/exec"

	shellquote "github.com/kballard/go-shellquote"
	"gitlab.com/tristan.cartledge/streamdeckplugin/logging"
)

func GetCommand(executable, arguments string) *exec.Cmd {
	args, err := shellquote.Split(arguments)
	if err != nil {
		// TODO should we communicate this back to stream deck?
		logging.Error("failed to parse arguments: %v", err)
		return nil
	}

	return exec.Command(executable, args...)
}
