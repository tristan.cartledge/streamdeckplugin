#!/usr/bin/env bash

BUILD_DIR=com.tristancartledge.plugin.sdPlugin

env GOOS=darwin GOARCH=amd64 go build -o ./${BUILD_DIR}/plugin
env GOOS=windows GOARCH=amd64 go build -o ./${BUILD_DIR}/plugin.exe

if [[ "$OSTYPE" == "darwin"* ]]; then
    PLUGIN_DIR=~/Library/Application\ Support/com.elgato.StreamDeck/Plugins/

    kill $(ps -ax | grep "Stream Deck.app" | awk '{print $1}')
    rm -rf ${PLUGIN_DIR}/${BUILD_DIR}
    cp -R ./${BUILD_DIR} "${PLUGIN_DIR}"
    open -a Stream\ Deck
else
    PLUGIN_DIR=/c/Users/trist/AppData/Roaming/Elgato/StreamDeck/Plugins

    STREAMDECK_PROCESS=$(tasklist | grep StreamDeck.exe | awk '{print $2}')
    PLUGIN_PROCESS=$(tasklist | grep ^plugin.exe | awk '{print $2}')
    taskkill //F //PID ${STREAMDECK_PROCESS}
    taskkill //F //PID ${PLUGIN_PROCESS}
    rm -rf ${PLUGIN_DIR}/${BUILD_DIR}
    cp -r ./${BUILD_DIR} ${PLUGIN_DIR}/
    "/c/Program Files/Elgato/StreamDeck/StreamDeck.exe"&
fi
