module gitlab.com/tristan.cartledge/streamdeckplugin

go 1.15

require (
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/kballard/go-shellquote v0.0.0-20180428030007-95032a82bc51
	github.com/valyala/fastjson v1.6.1
	gopkg.in/yaml.v2 v2.3.0 // indirect
	meow.tf/streamdeck/sdk v0.0.0-20200327060918-68fe819c6e62
)
